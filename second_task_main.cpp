#include <QCoreApplication>
#include <iostream>
#include <ctype.h>

using namespace std;

//структура узла
struct MyList
{
    int data; //информационное поле
    MyList *next; //указатель на следующий элемент
    MyList *prev; //указатель на предыдущий элемент
};

MyList *firstElement; //указатель на нулевой (первостоящий) элемент списка

//объявление функций
//главные функции
void addTopElement(int value);
void addLastElement(int value);
void addElementByIndex(int value, int position);
void deleteElement(int position);
void printElements();
void swapElements(MyList *element1, MyList *element2);
void sortAscending();

//вспомогательные к свапу
void swapExtremeElements(MyList* beginElement, MyList* endElement);
void swapNeighborElements(MyList* element1, MyList* element2);

//вспомогательные функции
MyList * getElementByPosition(int position);
bool isLast(MyList *a);
int thisListSize();
bool checkPositions(int position1, int position2);




int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int numberOfOperation;

    int value, position1, position2;
    cout<<"===================================="<<endl;
    do
    {
        cout<<"1. Add element to the top of list"<<endl;
        cout<<"2. Add element to the end of list"<<endl;
        cout<<"3. Add element by index"<<endl;
        cout<<"4. Delete element by index"<<endl;
        cout<<"5. Swap elements by indexes"<<endl;
        cout<<"6. Sort Ascending"<<endl;
        cout<<"7. Print list"<<endl;
        cout<<"0. Exit"<<endl;
        cout<<"\nPlease, enter number of operation: "; cin>>numberOfOperation;


        switch (numberOfOperation)
        {
        case 1:
            cout<<"data: ";
            if(!(cin>>value)) {
                cout<<"\nERROR...Incorrect data type!\n\n";
                cin.clear();
                cin.ignore();
            } else {
                addTopElement(value);
            }
            break;
        case 2:
            cout<<"data: ";
            if(!(cin>>value)) {
                cout<<"\nERROR...Incorrect data type!\n\n";
                cin.clear();
                cin.ignore();
            } else {
                addLastElement(value);
            }
            break;
        case 3:
            cout<<"data: ";
            if(!(cin>>value)) {
                cout<<"\nERROR...Incorrect data type!\n\n";
                cin.clear();
                cin.ignore();
            } else {
                cout<<"position in list: ";
                if(!(cin>>position1)) {
                    cout<<"\nERROR...Incorrect position type!\n\n";
                    cin.clear();
                    cin.ignore();
                } else {
                    addElementByIndex(value, position1);
                }
            }
            break;
        case 4:
            cout<<"position of deleted element: ";
            if(!(cin>>position1)) {
                cout<<"\nRROR...nIncorrect position type!\n\n";
                cin.clear();
                cin.ignore();
            } else {
                deleteElement(position1);
            }
            break;
        case 5:
            cout<<"position1: ";
            if(!(cin>>position1)) {
                cout<<"\nERROR...Incorrect position type!\n\n";
                cin.clear();
                cin.ignore();
            } else {
                cout<<"position2: ";
                if(!(cin>>position2)) {
                    cout<<"\nERROR...Incorrect position type!\n\n";
                    cin.clear();
                    cin.ignore();
                } else {
                    if(!checkPositions(position1, position2)) {
                        cout<<"\nERROR...Incorrect positions!\n\n";
                    } else {
                        MyList* element1 = getElementByPosition(position1);
                        MyList* element2 = getElementByPosition(position2);
                        swapElements(element1, element2);
                    }
                }
            }
            break;
        case 6:
            sortAscending();
            break;
        case 7:
            printElements();
            break;
        case 0:
            break;
        default:
            cout<<"\nIncorrect operation!\n"<<endl;
            break;
        }
        cout<<"===================================="<<endl;

    } while (numberOfOperation!=0);
    return a.exec();
}

void addTopElement(int value) {
    MyList *newElement=new MyList; //создаем новый элемент
    newElement->data=value;

    if (firstElement==NULL) //Если первого элемента нет
    {
        newElement->next=newElement; //установка указателя next
        newElement->prev=newElement; //установка указателя prev
        firstElement=newElement; //меняем голову списка на данный элемент
    } else {
        firstElement->prev->next = newElement;
        newElement->prev = firstElement->prev;
        firstElement->prev = newElement;
        newElement->next = firstElement;
        firstElement = newElement;
    }
    cout<<"\nElement added...\n\n";
}

void addLastElement(int value) {
    MyList *newElement=new MyList; //создаем новый элемент
    newElement->data=value;

    if (firstElement==NULL) //Если первого элемента нет
    {
        newElement->next=newElement; //установка указателя next
        newElement->prev=newElement; //установка указателя prev
        firstElement=newElement; //меняем голову списка на данный элемент
    } else {
        MyList *lastElement = firstElement->prev;
        lastElement->next = newElement;
        newElement->prev = lastElement;
        newElement->next = firstElement;
        firstElement->prev = newElement;
    }
    cout<<"\nElement added...\n\n";
}

void addElementByIndex(int value, int position)
{
    if(position < 0 || position > thisListSize()) {
        cout<<"\nERROR...Incorrect position!\n\n";
    } else {
        MyList *newElement=new MyList; //создаем новый элемент
        newElement->data=value;

        if (firstElement==NULL) //Если первого элемента нет
        {
            newElement->next=newElement; //установка указателя next
            newElement->prev=newElement; //установка указателя prev
            firstElement=newElement; //меняем голову списка на данный элемент
        } else {
            MyList *p = getElementByPosition(position); // находим в листе тот элемент который стоит на нужном нам месте
            if(position == 0) { //установка элемента на нулевую позицию
                firstElement = newElement; // меняем голову списка
            }
            p->prev->next=newElement; // вставляем элемент и перекидываем ссылки
            newElement->prev=p->prev;
            newElement->next=p;
            p->prev=newElement;
        }
        cout<<"\nElement added...\n\n";
    }
}


void deleteElement(int position)
{
    if(position < 0 || position >= thisListSize()) {
        cout<<"\nERROR...Incorrect position!\n\n";
    } else {
        if (firstElement==NULL) {
            cout<<"\nList is empty\n\n";
        } else if (firstElement==firstElement->next) {
            delete firstElement;
            firstElement=NULL;
        } else {
            MyList *a = getElementByPosition(position);
            if (position == 0){
                firstElement=a->next;
            }
            a->prev->next=a->next;
            a->next->prev=a->prev;
            delete a;
        }
        cout<<"\nElement deleted...\n\n";
    }
}

void printElements()
{
    if (firstElement==NULL) {
        cout<<"\nList is empty\n\n";
    } else {
        MyList *a=firstElement;
        cout<<"\nAll lements: ";
        do
        {
            cout<<a->data<<" ";
            a=a->next;
        } while(a!=firstElement);
        cout<<"\n\n";
    }
}

void swapElements(MyList *element1, MyList *element2)
{
    if(element1 != element2){
        MyList *temp = new MyList;
        //Перебрасываем ссылки
        if (element1->next == element2) {
            swapNeighborElements(element1, element2);
        } else if (element2->next == element1) {
            swapNeighborElements(element2, element1);
        } else if((element1 == firstElement && isLast(element2))) { // Если мы меняем местами крайние элементы то это немного особый случай
            swapExtremeElements(element1, element2);
        } else if((element2 == firstElement && isLast(element1))) {// Если мы меняем местами крайние элементы то это немного особый случай
            swapExtremeElements(element2, element1);
        } else {
            temp->next = element2->next;
            temp->prev = element2->prev;

            element2->next = element1->next;
            element1->next->prev = element2;
            element2->prev = element1->prev;
            element1->prev->next = element2;

            element1->next = temp->next;
            element1->next->prev = element1;
            element1->prev = temp->prev;
            element1->prev->next = element1;
        }
        if(element1 == firstElement) {
            firstElement = element2;
        } else if(element2 == firstElement) {
            firstElement = element1;
        }
    }
}

void sortAscending() {
    if (firstElement==NULL || thisListSize() == 1) {
        cout<<"\nNothing to sort\n\n";
    } else {
        int size = thisListSize();
        for (int i = 0; i < size; i++) {
            for(int j = (size - 1); j > i; j--){
                MyList* element1 = getElementByPosition(j-1);
                MyList* element2 = getElementByPosition(j);
                if(element1->data > element2->data) {
                    swapElements(element1, element2);
                }
            }
        }
    }
    cout<<"\nSorted...\n\n";
}


//вспомогательные к свапу
void swapExtremeElements(MyList* beginElement, MyList* endElement) { //функция для смены крайних значений
    MyList* temp = new MyList;
    temp->next= endElement->prev;

    endElement->next = beginElement->next;
    endElement->next->prev = endElement;

    endElement->prev = beginElement;
    beginElement ->next = endElement;

    beginElement->prev = temp->next;
    beginElement->prev->next = beginElement;
}

void swapNeighborElements(MyList* element1, MyList* element2) {
    MyList *temp = new MyList;
    temp->next = element2->next;
    temp->prev = element1->prev;

    element2->next = element1;
    temp->next->prev = element1;
    element1->next = temp->next;
    element2->prev = temp->prev;
    element1->prev = element2;
    temp->prev->next = element2;
}

//вспомогательные функции
bool isLast(MyList *a)
{
    if(firstElement->prev == a){
        return true;
    } else {
        return false;
    }
}

MyList* getElementByPosition(int position)
{
    MyList *p=firstElement;
    for(int i=0; i<position; i++) {
        p=p->next; // Доходим до нужной нам позиции
    }
    return p;
}

int thisListSize() {
    if (firstElement==NULL) {
        return 0;
    } else {
        int count = 0;
        MyList *a=firstElement;
        do
        {
            count++;
            a=a->next;
        } while(a!=firstElement);
        return count;
    }
}

bool checkPositions(int position1, int position2) {
    int lastIndex = thisListSize() - 1;
    if(position1 < 0 || position2 < 0 || position1 >= lastIndex || position2 >= lastIndex) {
        return false;
    } else {
        return true;
    }
}
